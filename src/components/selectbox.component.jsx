import { Form } from "react-bootstrap";

const SelectBox = ({ onChange, emptyLabel, entries }) => (
  <Form.Select onChange={onChange}>
    <option value="">{emptyLabel}</option>
    {entries.map((entry, index) => (
      <option key={index} value={entry}>
        {entry.charAt(0).toUpperCase() + entry.slice(1)}
      </option>
    ))}
  </Form.Select>
);

export default SelectBox;
