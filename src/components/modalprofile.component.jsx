import { Row, Container, Col, Modal, Image } from "react-bootstrap";
import ReactCountryFlag from "react-country-flag";

const ModalProfile = (props) => {
  const { user, ...stateProps } = props;

  return (
    <Modal size="xl" aria-labelledby="profile-modal" {...stateProps}>
      <Modal.Header closeButton>
        <Modal.Title id="profile-modal" className="text-center">
          {user.name.title} {user.name.first} {user.name.last}
        </Modal.Title>
      </Modal.Header>

      <Modal.Body>
        <Container className="show-grid">
          <Row>
            <Col sm={6} lg={4}>
              <h5>Identity</h5>
              <p>
                Title name : {user.name.title}
                <br />
                First name : {user.name.first}
                <br />
                Last name : {user.name.last}
                <br />
                Gender : {user.gender}
              </p>
              <p>
                Date of birth : {user.dob.date}
                <br />({user.dob.age} years)
                <br />
                Date of registration : {user.registered.date}
                <br />({user.registered.age} years)
              </p>
            </Col>

            <Col sm={6} lg={4}>
              <h5>Contact</h5>
              <p>
                Email : {user.email}
                <br />
                Phone : {user.phone}
                <br />
                Cell : {user.cell}
              </p>

              <p>
                Country : {user.location.country}{" "}
                <ReactCountryFlag countryCode={user.nat} />
                <br />
                State / Region : {user.location.state}
                <br />
                City : {user.location.city}
                <br />
                Street : {user.location.street.name}, n°
                {user.location.street.number}
              </p>
            </Col>
            <Col sm={12} lg={4} className="d-flex justify-content-center">
              <Image rounded src={user.picture.large} />
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default ModalProfile;
