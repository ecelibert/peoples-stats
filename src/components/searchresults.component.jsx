import { Link } from "react-router-dom";

import { useEffect, useState } from "react";
import { Row, Container, Col, Card, Button } from "react-bootstrap";

// import ModalProfile from "./modalprofile.component";

const SearchResults = ({ filteredUsers }) => {
  const maxUsersPerPage = 20;

  const [usersPage, setUsersPage] = useState([]);
  const [page, setPage] = useState(1);

  const minPage = 1;
  const [maxPage, serMaxPage] = useState(minPage);

  useEffect(() => {
    const newUsersPage = filteredUsers.slice(
      maxUsersPerPage * (page - 1),
      maxUsersPerPage * page
    );

    setUsersPage(newUsersPage);
    serMaxPage(Math.floor(filteredUsers.length / maxUsersPerPage) + 1);

    if (!filteredUsers[(page - 1) * maxUsersPerPage + 1]) {
      // If the page is out of bounds of the changed array is -> reset the page
      setPage(1);
    }
  }, [filteredUsers, page]);

  const handleClickPage = (val) => {
    setPage(page + val);
  };

  // const [profileUser, setProfileUser] = useState(filteredUsers[0]); // filteredUsers[0] : temporary value
  // const [showProfile, setShowProfile] = useState(false);

  // const handleClickProfile = (user) => {
  //   setProfileUser(user);
  //   setShowProfile(true);
  // };

  const PageNav = () => (
    <Row className="d-flex justify-content-center">
      <Col className="d-flex justify-content-center">
        {page > minPage && (
          <Button
            variant="secondary"
            size="sm"
            onClick={() => handleClickPage(-1)}
          >
            Previous Page
          </Button>
        )}
      </Col>
      <Col>
        <p className="text-center">Page {page}</p>
      </Col>
      <Col className="d-flex justify-content-center">
        {page < maxPage && (
          <Button
            variant="secondary"
            size="sm"
            onClick={() => handleClickPage(+1)}
          >
            Next Page
          </Button>
        )}
      </Col>
    </Row>
  );

  return (
    <Container>
      <PageNav />

      <Row className="mt-3 mb-4 d-flex justify-content-center">
        {usersPage.map((user, index) => (
          <Col
            sm={6}
            md={4}
            xl={3}
            key={index}
            className="mt-3 d-flex justify-content-center"
          >
            <Card style={{ width: "17rem" }}>
              <Card.Img variant="top" src={user.picture.large} />
              <Card.Body>
                <Card.Title>
                  {user.name.title} {user.name.first} {user.name.last}
                </Card.Title>
                <Card.Text>
                  Email : {user.email}
                  <br />
                  Phone : {user.phone}
                  <br />
                  Cell : {user.cell}
                  <br />
                  {/* <Button
                    variant="primary"
                    className="mt-3"
                    onClick={() => handleClickProfile(user)}
                  >
                    Open profile
                  </Button> */}
                  <Link
                    to={"/search/" + user.id}
                    role="button"
                    className="mt-2 btn btn-primary"
                  >
                    Go to profile
                  </Link>
                </Card.Text>
              </Card.Body>
            </Card>
          </Col>
        ))}
      </Row>

      {/* <ModalProfile
        user={profileUser}
        show={showProfile}
        onHide={() => setShowProfile(false)}
      /> */}

      <PageNav />
    </Container>
  );
};

export default SearchResults;
