import { Map, GoogleApiWrapper, Marker } from "google-maps-react";
import { useNavigate } from "react-router-dom";

const MapContainer = (props) => {
  const mapStyles = {
    width: "97.5%",
    height: "100%",
  };

  const zoomLevel = props.users.length === 1 ? 6 : 3;
  const center = {
    lat: props.users[0].location.coordinates.latitude,
    lng: props.users[0].location.coordinates.longitude,
  };

  const displayMarkers = () => {
    return props.users.map((user, index) => {
      return (
        <Marker
          key={index}
          id={index}
          position={{
            lat: user.location.coordinates.latitude,
            lng: user.location.coordinates.longitude,
          }}
          onClick={() => handleClick(user)}
        />
      );
    });
  };

  const navigate = useNavigate();
  const handleClick = (user) => navigate("/search/" + user.id);

  return (
    <Map
      google={props.google}
      zoom={zoomLevel}
      style={mapStyles}
      initialCenter={center}
    >
      {displayMarkers()}
    </Map>
  );
};

export default GoogleApiWrapper({
  apiKey: "AIzaSyDXkwtfJoz1dwcUAtVQpXEvUP1dlYexXOE",
})(MapContainer);
