import { useState, useEffect, Fragment } from "react";

const Datetime = () => {
  const locale = "fr";
  const [today, setDate] = useState(new Date());

  useEffect(() => {
    const timer = setInterval(() => {
      setDate(new Date());
    }, 1000);
    return () => {
      clearInterval(timer);
    };
  }, []);

  let dayOfWeek = today.toLocaleDateString(locale, { weekday: "long" });
  dayOfWeek = dayOfWeek.charAt(0).toUpperCase() + dayOfWeek.slice(1);

  const date = `${dayOfWeek} ${today.getDate()} ${today.toLocaleDateString(
    locale,
    {
      month: "long",
    }
  )}`;

  const time = today.toLocaleTimeString(locale, {
    hour12: false,
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
  });

  return (
    <Fragment>
      {date} | {time}
    </Fragment>
  );
};

export default Datetime;
