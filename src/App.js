import { Routes, Route } from "react-router-dom";

import { Container } from "react-bootstrap";

import Navigation from "./routes/navigation.component";
import Home from "./routes/home.component";
import Dashboard from "./routes/dashboard.component";
import Search from "./routes/search.component";
import SearchUser from "./routes/searchuser.component";
import Signin from "./routes/signin.component";
import NotFound from "./routes/notfound.component";

const App = () => {
  return (
    <div className="App">
      <Container fluid="true">
        <Routes>
          <Route path="/" element={<Navigation />}>
            <Route index element={<Home />} />
            <Route path="dashboard" element={<Dashboard />} />
            <Route path="search" element={<Search />} />
            <Route path="search/:userId" element={<SearchUser />} />
            <Route path="signin" element={<Signin />} />
            <Route path="*" element={<NotFound />} />
          </Route>
        </Routes>
      </Container>
    </div>
  );
};

export default App;
