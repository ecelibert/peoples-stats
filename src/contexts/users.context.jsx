import { createContext, useState } from "react";
import userData from "../data/users.json";

for (let i = 0; i < userData.length; i++) {
  delete userData[i].id;
  userData[i].id = i + 1;
}

export const UsersContext = createContext({
  users: userData,
  setUsers: () => null,
});

export const UsersProvider = ({ children }) => {
  const [users, setUsers] = useState(userData);
  const value = { users, setUsers };

  return (
    <UsersContext.Provider value={value}>{children}</UsersContext.Provider>
  );
};
