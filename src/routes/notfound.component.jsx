import { Container } from "react-bootstrap";

const NotFound = () => {
  return (
    <Container className="m-3">
      <h1>404 - Page not found</h1>
      <h4 className="text-center mt-4">
        The page you're looking for does not seem to be here... 🤔
      </h4>
    </Container>
  );
};

export default NotFound;
