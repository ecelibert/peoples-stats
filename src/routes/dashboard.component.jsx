import { useContext } from "react";
import { UsersContext } from "../contexts/users.context";

import { Row, Col, Table, Container } from "react-bootstrap";
import ReactCountryFlag from "react-country-flag";

import DoughnutChart from "../components/doughtnutchart.component";
import MapContainer from "../components/mapcontainer.component";

const Dashboard = () => {
  const { users } = useContext(UsersContext);

  // Gender Chart

  let genders = [];

  for (const user of users) {
    if (!genders.includes(user.gender)) genders.push(user.gender);
  }

  let percentUsersGenders = [];

  for (const gender of genders) {
    percentUsersGenders.push(
      (users.filter((u) => u.gender === gender).length / users.length) * 100
    );
  }

  const dataGender = {
    labels: genders,
    datasets: [
      {
        label: "",
        data: percentUsersGenders,
        backgroundColor: ["rgba(255, 99, 132, 0.5)", "rgba(54, 162, 235, 0.5)"],
        borderColor: ["rgba(255, 99, 132, 1)", "rgba(54, 162, 235, 1)"],
      },
    ],
  };

  // Countries Chart

  let countries = [];

  for (const user of users) {
    if (!countries.find((c) => c.name === user.location.country)) {
      countries.push({
        name: user.location.country,
        code: user.nat,
      });
    }
  }

  let percentUsersContries = [];

  for (const country of countries) {
    percentUsersContries.push(
      (users.filter((u) => u.location.country === country.name).length /
        users.length) *
        100
    );
  }

  const dataContries = {
    labels: countries.map((country) => country.name),
    datasets: [
      {
        label: "",
        data: percentUsersContries,
        backgroundColor: [
          "rgb(224, 179, 54, 0.5)",
          "rgb(27, 218, 170, 0.5)",
          "rgb(27, 169, 76, 0.5)",
          "rgb(183, 36, 31, 0.5)",
          "rgb(73, 85, 230, 0.5)",
          "rgb(76, 112, 67, 0.5)",
          "rgb(25, 253, 134, 0.5)",
          "rgb(33, 196, 157, 0.5)",
          "rgb(159, 118, 117, 0.5)",
          "rgb(232, 51, 206, 0.5)",
          "rgb(53, 69, 236, 0.5)",
          "rgb(241, 67, 65, 0.5)",
          "rgb(231, 133, 125, 0.5)",
          "rgb(233, 163, 169, 0.5)",
          "rgb(103, 199, 28, 0.5)",
          "rgb(24, 65, 11, 0.5)",
          "rgb(247, 207, 36, 0.5)",
          "rgb(163, 95, 120, 0.5)",
          "rgb(21, 85, 142, 0.5)",
          "rgb(140, 149, 66, 0.5)",
        ],
        borderColor: [
          "rgb(224, 179, 54, 1)",
          "rgb(27, 218, 170, 1)",
          "rgb(27, 169, 76, 1)",
          "rgb(183, 36, 31, 1)",
          "rgb(73, 85, 230, 1)",
          "rgb(76, 112, 67, 1)",
          "rgb(25, 253, 134, 1)",
          "rgb(33, 196, 157, 1)",
          "rgb(159, 118, 117, 1)",
          "rgb(232, 51, 206, 1)",
          "rgb(53, 69, 236, 1)",
          "rgb(241, 67, 65, 1)",
          "rgb(231, 133, 125, 1)",
          "rgb(233, 163, 169, 1)",
          "rgb(103, 199, 28, 1)",
          "rgb(24, 65, 11, 1)",
          "rgb(247, 207, 36, 1)",
          "rgb(163, 95, 120, 1)",
          "rgb(21, 85, 142, 1)",
          "rgb(140, 149, 66, 1)",
        ],
      },
    ],
  };

  // Table of the 15 most populated contries

  let totalUsersContries = [];
  let id = 1;

  for (const country of countries) {
    totalUsersContries.push({
      id,
      country: country,
      users: users.filter((u) => u.location.country === country.name).length,
    });
    id++;
  }

  const sortedCountries = JSON.parse(JSON.stringify(totalUsersContries))
    .sort((a, b) => (a.users > b.users ? -1 : 1))
    .slice(0, 15);

  // Data of the firsts 100 users for the Google Map :

  const firstsUsers = users.slice(0, 100);

  return (
    <Container fluid className="m-3">
      <h1>Dashboard</h1>

      <Row>
        <h4 className="mt-2 mb-5 text-center">
          There is a total of {users.length} users in the database.
        </h4>

        <Col sm={12} md={{ offset: 2, span: 4 }}>
          <p className="text-center">Percentage of genders of users</p>
          <DoughnutChart data={dataGender} />
        </Col>

        <Col sm={12} md={4}>
          <p className="text-center">Percentage of countries of users</p>
          <DoughnutChart data={dataContries} />
        </Col>

        <Col sm={12} className="mt-5 text-center">
          <h4>
            The 15 most populated countries of the users from our data are :
          </h4>

          <Container className="mt-4">
            <Table striped bordered>
              <thead>
                <tr>
                  <th>Flag</th>
                  <th>Country Name </th>
                  <th>Nb Users</th>
                </tr>
              </thead>
              <tbody>
                {sortedCountries.map((sortedCountry) => (
                  <tr key={sortedCountry.id}>
                    <td>
                      <ReactCountryFlag
                        countryCode={sortedCountry.country.code}
                      />
                    </td>
                    <td>{sortedCountry.country.name}</td>
                    <td>{sortedCountry.users}</td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Container>
        </Col>
      </Row>

      <Row>
        <Col sm={12} className="mt-5 text-center">
          <h4>The locations of the firsts 100 users are :</h4>
          <MapContainer users={firstsUsers} />
        </Col>
      </Row>
    </Container>
  );
};

export default Dashboard;
