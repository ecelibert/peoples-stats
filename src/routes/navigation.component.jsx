import { Container, Nav, Navbar } from "react-bootstrap";
import { Link, Outlet } from "react-router-dom";

import Datetime from "../components/datetime.component";

const Navigation = () => {
  return (
    <header className="App-header">
      <Navbar bg="dark" variant="dark" className="mb-3">
        <Container>
          <Navbar.Brand as={Link} to="/">
            People Stats
          </Navbar.Brand>
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/dashboard">
              Dashboard
            </Nav.Link>
            <Nav.Link as={Link} to="/search">
              Search
            </Nav.Link>
          </Nav>

          <Nav className="mx-3">
            <Nav.Link as={Link} to="/signin">
              Sign in
            </Nav.Link>
          </Nav>
          <Navbar.Text>
            <Datetime />
          </Navbar.Text>
        </Container>
      </Navbar>

      <Outlet />
    </header>
  );
};

export default Navigation;
