import { useParams } from "react-router-dom";

import { useContext } from "react";
import { UsersContext } from "../contexts/users.context";

import { Container, Row, Col, Image } from "react-bootstrap";
import ReactCountryFlag from "react-country-flag";
import MapContainer from "../components/mapcontainer.component";

const SearchUser = () => {
  let { userId } = useParams();
  const { users } = useContext(UsersContext);
  const user = users[userId - 1];

  return (
    <Container fluid>
      <h1>
        {user.name.title} {user.name.first} {user.name.last}
      </h1>

      <Row className="mt-5 mb-4">
        <Col sm={6} lg={4}>
          <h5>Identity</h5>
          <p>
            Title name : {user.name.title}
            <br />
            First name : {user.name.first}
            <br />
            Last name : {user.name.last}
            <br />
            Gender : {user.gender}
          </p>
          <p>
            Date of birth : {user.dob.date}
            <br />({user.dob.age} years)
            <br />
            Date of registration : {user.registered.date}
            <br />({user.registered.age} years)
          </p>
        </Col>

        <Col sm={6} lg={4}>
          <h5>Contact</h5>
          <p>
            Email : {user.email}
            <br />
            Phone : {user.phone}
            <br />
            Cell : {user.cell}
          </p>

          <p>
            Country : {user.location.country}{" "}
            <ReactCountryFlag countryCode={user.nat} />
            <br />
            State / Region : {user.location.state}
            <br />
            City : {user.location.city}
            <br />
            Street : {user.location.street.name}, n°
            {user.location.street.number}
          </p>
        </Col>
        <Col sm={12} lg={4} className="d-flex justify-content-center">
          <Image rounded src={user.picture.large} />
        </Col>
      </Row>

      <MapContainer users={[user]} />
    </Container>
  );
};

export default SearchUser;
