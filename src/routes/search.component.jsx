import { useContext, useState, useEffect } from "react";
import { UsersContext } from "../contexts/users.context";

import { Row, Container, Form, Col } from "react-bootstrap";

import SearchResults from "../components/searchresults.component";
import SelectBox from "../components/selectbox.component";

const Search = () => {
  // Base array

  const { users } = useContext(UsersContext);

  // Creating filters's arrays and states

  let genders = [];
  for (const user of users) {
    if (!genders.includes(user.gender)) genders.push(user.gender);
  }
  genders.sort((a, b) => a.localeCompare(b));

  let cities = [];
  for (const user of users) {
    if (!cities.includes(user.location.city)) cities.push(user.location.city);
  }
  cities.sort((a, b) => a.localeCompare(b));

  let states = [];
  for (const user of users) {
    if (!states.includes(user.location.state)) states.push(user.location.state);
  }
  states.sort((a, b) => a.localeCompare(b));

  const [searchField, setSearchField] = useState("");
  const [selectGender, setselectGender] = useState("");
  const [selectCity, setselectCity] = useState("");
  const [selectState, setselectState] = useState("");

  const [filteredUsers, setFilterUsers] = useState(users);

  const onSearchFieldChange = (event) => {
    const searchFieldString = event.target.value.toLocaleLowerCase();
    setSearchField(searchFieldString);
  };

  const onSelectGenderChange = (event) => {
    const selectString = event.target.value;
    setselectGender(selectString);
  };

  const onSelectCityChange = (event) => {
    const selectString = event.target.value;
    setselectCity(selectString);
  };

  const onSelectStateChange = (event) => {
    const selectString = event.target.value;
    setselectState(selectString);
  };

  // Filtring the base array

  useEffect(() => {
    const newFilteredUsers = users.filter((user) => {
      return (
        (user.name.first.toLocaleLowerCase().includes(searchField) ||
          user.name.last.toLocaleLowerCase().includes(searchField) ||
          user.phone.includes(searchField)) &&
        (user.gender === selectGender || selectGender === "") &&
        (user.location.city === selectCity || selectCity === "") &&
        (user.location.state === selectState || selectState === "")
      );
    });

    setFilterUsers(newFilteredUsers);
  }, [users, searchField, selectGender, selectCity, selectState]);

  // Boolean for showing or not the results (there must be at least 1 active filter and at least 1 user to display)

  const showUsers =
    (searchField !== "" ||
      selectGender !== "" ||
      selectCity !== "" ||
      selectState !== "") &&
    filteredUsers.length;

  return (
    <Container fluid className="m-3">
      <Row>
        <h1>Search</h1>
        <h4 className="mt-2 mb-4 text-center">
          Search from {users.length} users
        </h4>
      </Row>

      <Row className="d-flex justify-content-center">
        <Col sm={2}>
          <SelectBox
            onChange={onSelectGenderChange}
            emptyLabel="Select a gender"
            entries={genders}
          />
        </Col>

        <Col sm={2}>
          <SelectBox
            onChange={onSelectStateChange}
            emptyLabel="Select a state / region"
            entries={states}
          />
        </Col>

        <Col sm={2}>
          <SelectBox
            onChange={onSelectCityChange}
            emptyLabel="Select a city"
            entries={cities}
          />
        </Col>
      </Row>

      <Row className="d-flex justify-content-center">
        <Col sm={6}>
          <Form className="mt-4 mb-4">
            <Form.Control
              type="search"
              placeholder="Search Names or Phones numbers"
              onChange={onSearchFieldChange}
            />
          </Form>
        </Col>
      </Row>

      {filteredUsers.length !== users.length && (
        <h4 className="text-center mb-3">
          Search Results : {filteredUsers.length}
        </h4>
      )}

      {showUsers ? <SearchResults filteredUsers={filteredUsers} /> : null}
    </Container>
  );
};

export default Search;
