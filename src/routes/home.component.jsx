import { Col, Container, Row } from "react-bootstrap";
import { Link } from "react-router-dom";

import { ReactComponent as IconChartSvg } from "../icon-charts.svg";

const Home = () => {
  return (
    <Container fluid className="m-3">
      <h1>Welcome to People Stats !</h1>

      <Row className="mt-3">
        <Col className="d-flex justify-content-center align-items-center">
          <div className="m-5 text-center">
            <p>
              Peoples stats is a start-up with the concept of storing a large
              number of users on which it is possible to perform statistics on
              the data points they contain !
            </p>
            <p>
              For example, this application allows you to know in real time the
              number of women and men present in the company's database, in the{" "}
              <Link to="/dashboard">Dashboard</Link> page.
            </p>
          </div>
        </Col>
        <Col className="d-flex justify-content-center align-items-center">
          <IconChartSvg width="32rem" />
        </Col>
      </Row>

      <Row className="mt-3">
        <Col className="text-center">
          <a href={"https://www.streamlinehq.com"}>
            Free Charts Pie And Bars SVG illustration by Streamline
          </a>
        </Col>
      </Row>
    </Container>
  );
};

export default Home;
